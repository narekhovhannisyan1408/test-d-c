import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';


@Injectable()
export class MainService {
  public posts = new BehaviorSubject(null);

  constructor() {

  }

  getPosts() {
    return this.posts;
  }

  setPosts(data: any) {
    this.posts.next(data);
  }

}
