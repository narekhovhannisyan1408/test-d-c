import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { User } from "../interfaces/user.interface";


@Injectable({
  providedIn: 'root'
})
export class AppService {
  public users = new BehaviorSubject(null);

  constructor() {

  }

  getUsers() {
    return this.users;
  }

  setUsers(data: any) {
    this.users.next(data);
  }

}
