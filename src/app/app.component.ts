import { AfterViewInit, Component, OnInit } from '@angular/core';
import { LoadingService } from './services/loading.service';
import users from './constants/users.json'
import posts from './constants/posts.json'
import { Router } from "@angular/router";
import { AppService } from "./services/app.service";
import { User } from "./interfaces/user.interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  public title = 'test-app';
  public loading$ = this.loader.loading$;
  public data: Array<User>;

  constructor(private loader: LoadingService,
              private router: Router,
              private appSvc: AppService) {
    this.loader.show();
    this.data = users.map(t1 => ({...t1, posts: posts.filter(t2 => t2.userID === t1.id)}))
    this.appSvc.setUsers(this.data);
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.router.navigate(['/main']).then(() => {
        this.loader.hide();
      });
    }, 1000);
  }
}
