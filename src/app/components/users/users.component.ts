import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from "../../services/app.service";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { MainService } from "../../services/main.service";
import { User } from "../../interfaces/user.interface";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  public users: Array<User> | undefined;
  private sub = new Subscription();

  constructor(private appSvc: AppService,
              private mainSvc: MainService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.sub.add(this.appSvc.getUsers().subscribe((users: Array<User> | null) => {
      if (users) {
        this.users = users;
      }
    }));
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  redirect(item: User) {
    this.mainSvc.setPosts(item);
    this.router.navigate(['../posts'], {relativeTo: this.activatedRoute}).then();
  }
}
