import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { appConfig } from "../../constants/app.constant";
import { UsersComponent } from "../users/users.component";
import { PostsComponent } from "../posts/posts.component";


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: 'users',
        pathMatch: 'full'
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'posts',
        component: PostsComponent,
      }
    ]
  },
  {path: '**', redirectTo: appConfig.defaultHomePage},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
