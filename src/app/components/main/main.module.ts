import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { SharedModule } from "../../shared/shared.module";
import { UsersComponent } from "../users/users.component";
import { PostsComponent } from "../posts/posts.component";
import { MainService } from "../../services/main.service";


@NgModule({
  imports: [
    MainRoutingModule,
    SharedModule
  ],
  declarations: [
    MainComponent,
    UsersComponent,
    PostsComponent
  ],
  providers: [MainService],
  entryComponents: []
})
export class MainModule {
}
