import { Component, OnDestroy, OnInit } from '@angular/core';
import { MainService } from "../../services/main.service";
import { Subscription } from "rxjs";
import { User } from "../../interfaces/user.interface";
import { ExportToCsv } from 'export-to-csv';
import * as htmlToImage from 'html-to-image';
import download from 'downloadjs'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {
  public user: User | undefined;
  private sub = new Subscription();

  constructor(private mainSvc: MainService,) {
    this.sub.add(this.mainSvc.getPosts().subscribe((user: User | null) => {
      if (user) {
        this.user = user;
      }
    }));
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  exportCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      filename: `${this.user?.firstName} ${this.user?.lastName} posts`,
      showLabels: true,
      showTitle: false,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: false,
      headers: ['Messages']
    };
    const csvExporter = new ExportToCsv(options);
    const posts = this.user?.posts?.map(it => {
      return {post: it.post}
    });
    csvExporter.generateCsv(posts);
  }

  exportImage() {
    const node:any = document.getElementById('posts');
    const self = this;
    htmlToImage.toPng(node)
      .then(function (dataUrl: string) {
        download(dataUrl, `${self.user?.firstName} ${self.user?.lastName} posts.png`);
      })
      .catch(function (error) {
        console.error('oops, something went wrong!', error);
      });
  }

}
