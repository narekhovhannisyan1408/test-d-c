import { Post } from "./post.interface";

export interface User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  color: string;
  posts?: Array<Post>;
}
