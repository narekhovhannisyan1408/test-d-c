export interface Post {
  id: number;
  userID: number;
  post: string;
}
